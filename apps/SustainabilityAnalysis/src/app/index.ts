import { LighthouseTool, LighthouseReport } from "../infra/lighthouse"

import { Recomendation } from "data/entities/recomendation"
import type { Report } from "data/entities/tool"



const start = async () => {
  // Start our tools
  const lh = new LighthouseTool()
  const url = "https://almanac.httparchive.org/en/2022/sustainability"
  await lh.execute(url)
  
  // Create recomendations
  const recomendations: Array<Recomendation> = []
  recomendations.push(
    new Recomendation(
      "Provide a text alternative to multimedia content",
      lh,
      (data: Report): boolean => {
        const lhReport = data as LighthouseReport
        const audits = lhReport.lhr['audits']
        console.log(audits)
        audits['image-alt'].score;
        return true
      }
    )
  )
  
  
  recomendations.forEach(recomendation => {
    console.log(`Resomendation: ${recomendation.getName()}`)
    let result = recomendation.eval()
    console.log(result)
  })
}

start()