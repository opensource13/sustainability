// Lighthouse dependencies
import lighthouse from 'lighthouse'
import {launch} from 'chrome-launcher'

// Interfaces
import type {Tool, Report} from "data/entities/tool"

export interface LighthouseReport extends Report {
  lhr: any
}

export class LighthouseTool implements Tool {

  report: LighthouseReport | null = null
  
  async execute(url: string) {
    try {
      const chrome = await launch({chromeFlags: ['--headless']});
      const options = {output: 'json', port: chrome.port};
      const runnerResult = await lighthouse(url, options)
        .catch((e: Error) => {throw Error("Lighthouse: Lighthouse Problem")});;

      const fetchTime: Date = new Date(runnerResult.lhr.fetchTime)

      const results: LighthouseReport = {
        page: "Check for chrome not able to run on docker linux" + url,
        tmstp: fetchTime, 
        lhr: runnerResult.lhr
      }

      await chrome.kill();
      this.report = results
    }
    catch (e) {
      console.log(e)
      throw Error("not able to get results from parser har")
    }
  }


} 