
// [comment-AZ]: Followed directions from https://github.com/GoogleChrome/lighthouse/issues/1773
declare module 'lighthouse' {
  function lighthouse(
    url: string,
    options: Partial<LH.CliFlags>,
  ): Promise<LH.RunnerResult>;
  export = lighthouse;
}