import { isThisTypeNode } from "typescript"
import type {Tool, Report} from "./tool"


export class Recomendation {
  constructor(
    private readonly name: string,
    private readonly tool: Tool,
    private readonly evaluateFn: (data: Report) => boolean
  ) {}

  getName() {
    return this.name
  }

  eval() {
    if (this.tool.report) {
      return this.evaluateFn(this.tool.report)
    }
  }
}
