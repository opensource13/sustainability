/*
 * This interface define what methods should implement any third party tool 
 * used for the analzysis
 */
export interface Tool {
  report: Report | null

  execute(url: string): void
}

/*
 *  This interface defines a basic structure of a report that a tool generates
 */

export interface Report {
  page: string
  tmstp?: Date
  artifacts?: any
}
